# jkarma-products-pbcd-demo

Demo project using jKarma for detecting changes on the customer purchases history. The problem is solved by means of a PBCD algorithm built on top of a customized pattern mining strategy.